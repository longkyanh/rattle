#' Build a Linear model.
#' 
#' Time-stamp: <2017-02-12 22:06:58 Graham Williams>
#'
executeModelRxBTrees <- function()
{
  # Initial setup. 
  
  TV <- "ada_textview"
  VAR <- "crs$ada"    
  
  # Formula Creation for the model
  frml <- paste(crs$target, "~", paste(crs$input, collapse=" + "))
  # Build the model
  model.cmd <- paste0(VAR," <- ",sprintf("rxBTrees(formula = %s, data = %s, maxDepth = 30, cp = 0.01,             minSplit = 20)", frml, "crs$xdf.split[[1]]"))	   

    
  
  # Build the model.

  appendLog(Rtxt("Build a rxBTrees model."),
            model.cmd, sep="")
  start.time <- Sys.time()
  result <- try(eval(parse(text=model.cmd)), silent=TRUE)
  summary.cmd <- "print(summary(crs$ada))"
  
  return(TRUE)
}
